import { getTranslation, i18n } from "@/config";
import type { PriceRecurrenceType } from "@/money/priceRecurrenceType";
import type { PricingType } from "@/money/pricingType";
import { type Dinero, toDecimal } from "dinero.js";

export default class MoneyFormatter {
  constructor(private locale: string = i18n.config.globalProperties.locale) {}

  format(money: Dinero<number>, pricingType?: PricingType, priceRecurrenceType?: PriceRecurrenceType): string {
    const formattedMoney = [
      toDecimal(money, ({ value, currency }) => {
        return Number(value).toLocaleString(this.locale, {
          style: "currency",
          currencyDisplay: "narrowSymbol",
          currency: currency.code,
        });
      }),
    ];

    if (pricingType)
      formattedMoney.push(getTranslation(`shared.domain.SharedKernel_PricingType.${pricingType}.label_short`));
    if (priceRecurrenceType)
      formattedMoney.push(
        getTranslation(`shared.domain.Quote_PriceRecurrenceType.${priceRecurrenceType}.label_suffix`),
      );

    return formattedMoney.join(" ");
  }
}
