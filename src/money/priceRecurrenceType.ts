export enum PriceRecurrenceType {
  NONE = "NONE",
  DAILY = "DAILY", // For later maybe, as we display something similar on client quote
  MONTHLY = "MONTHLY",
}
