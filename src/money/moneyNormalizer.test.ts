import { expect, test, describe } from "bun:test";
import MoneyNormalizer from "./moneyNormalizer";

describe("MoneyNormalizer", () => {
  test("normalizes EUR amount from string", () => {
    const money = MoneyNormalizer.fromString("EUR 123.45");
    const normalized = new MoneyNormalizer(money);
    expect(normalized.toString()).toBe("EUR 123.45");
  });

  test("normalizes GBP amount from string", () => {
    const money = MoneyNormalizer.fromString("GBP 99.99");
    const normalized = new MoneyNormalizer(money);
    expect(normalized.toString()).toBe("GBP 99.99");
  });

  test("throws error for unsupported currency", () => {
    expect(() => {
      MoneyNormalizer.fromString("XXX 100.00");
    }).toThrow("Unsupported currency code");
  });
});
