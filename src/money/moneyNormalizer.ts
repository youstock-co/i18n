import { dinero, type Dinero, toDecimal } from "dinero.js";
import * as Currency from "@dinero.js/currencies";

export const CurrencySymbols = {
  [Currency.EUR.code]: "€",
  [Currency.GBP.code]: "£",
};

export default class MoneyNormalizer {
  constructor(public money: Dinero<number>) {}

  /**
   * @param str "CODE value" notation. eg : "USD 234.00"
   */
  static fromString(str: string) {
    const [code, value] = str.split(" ");

    const currency = Object.values(Currency).find((v) => v.code === code);

    if (!currency) throw new Error(`Unsupported currency code : "${code}"`);

    return dinero({ currency, amount: Number.parseInt(value.replaceAll(".", "")), scale: 2 });
  }

  toString() {
    return toDecimal(this.money, (val) => {
      return `${val.currency.code} ${val.value}`;
    });
  }
}
