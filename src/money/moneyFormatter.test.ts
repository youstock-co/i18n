import { expect, test, describe } from "bun:test";
import { dinero } from "dinero.js";
import * as Currency from "@dinero.js/currencies";
import MoneyFormatter from "./moneyFormatter";
import { PricingType } from "./pricingType";
import { PriceRecurrenceType } from "./priceRecurrenceType";

describe("MoneyFormatter", () => {
  test("formats EUR amount in French locale", () => {
    const money = dinero({ amount: 12345, currency: Currency.EUR, scale: 2 });
    const formatter = new MoneyFormatter("fr-FR");
    const result = formatter.format(money);
    expect(result).toBe("123,45\u00A0€");
  });

  test("formats EUR amount in English locale", () => {
    const money = dinero({ amount: 12345, currency: Currency.EUR, scale: 2 });
    const formatter = new MoneyFormatter("en-GB");
    const result = formatter.format(money);
    expect(result).toBe("€123.45");
  });

  test("formats with pricing type", () => {
    const money = dinero({ amount: 12345, currency: Currency.EUR, scale: 2 });
    const formatter = new MoneyFormatter("fr-FR");
    const result = formatter.format(money, PricingType.EXCLUDING_TAX);
    expect(result).toBe("123,45\u00A0€ HT");
  });

  test("formats with price recurrence", () => {
    const money = dinero({ amount: 12345, currency: Currency.EUR, scale: 2 });
    const formatter = new MoneyFormatter("fr-FR");
    const result = formatter.format(money, undefined, PriceRecurrenceType.MONTHLY);
    expect(result).toBe("123,45\u00A0€ / mois");
  });
});
