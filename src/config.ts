export const i18n = {
  config: {
    globalProperties: {
      locale: "fr-FR",
    },
    use12HourFormat: ["en-GB", "en-US"],
    translations: {
      "fr-FR": {
        "shared.domain.SharedKernel_PricingType.EXCLUDING_TAX.label_short": "HT",
        "shared.domain.SharedKernel_PricingType.INCLUDING_TAX.label_short": "TTC",
        "shared.domain.Quote_PriceRecurrenceType.DAILY.label_suffix": "/ jour",
        "shared.domain.Quote_PriceRecurrenceType.MONTHLY.label_suffix": "/ mois",
      },
      "en-GB": {
        "shared.domain.SharedKernel_PricingType.EXCLUDING_TAX.label_short": "Excl. VAT",
        "shared.domain.SharedKernel_PricingType.INCLUDING_TAX.label_short": "Incl. VAT",
        "shared.domain.Quote_PriceRecurrenceType.DAILY.label_suffix": "/ day",
        "shared.domain.Quote_PriceRecurrenceType.MONTHLY.label_suffix": "/ month",
      },
    },
  },
};

export function getTranslation(key: string, locale?: string): string {
  const currentLocale = (locale ?? i18n.config.globalProperties.locale) as keyof typeof i18n.config.translations;
  const keyPath = key as keyof (typeof i18n.config.translations)[typeof currentLocale];

  return i18n.config.translations[currentLocale]?.[keyPath];
}
