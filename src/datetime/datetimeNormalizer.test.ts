import { expect, test, describe } from "bun:test";
import { DatetimeNormalizer } from "./datetimeNormalizer";
import dayjs from "dayjs";
import timezone from "dayjs/plugin/timezone";

dayjs.extend(timezone);

describe("DatetimeNormalizer", () => {
  const testDateStr = "2024-03-20T14:30:00:000";
  const testTimeZone = "Europe/Paris";

  test("normalizes from string", () => {
    const date = DatetimeNormalizer.fromString(testDateStr);
    expect(DatetimeNormalizer.toString(date)).toBe(testDateStr);
  });

  test("normalizes from object", () => {
    const input = {
      dateTime: testDateStr,
      timeZone: testTimeZone,
    };
    const date = DatetimeNormalizer.fromObject(input);
    const output = DatetimeNormalizer.toObject(date);
    expect(output.dateTime).toBe(testDateStr);
    expect(output.timeZone).toBe(testTimeZone);
  });

  test("maintains UTC in string conversion", () => {
    const date = DatetimeNormalizer.fromString(testDateStr);
    expect(date.isUTC()).toBe(true);
  });
});
