import { expect, test, describe } from "bun:test";
import dayjs from "dayjs";
import DatetimeFormatter, { DateFormatStyle, TimeFormatStyle } from "./datetimeFormatter";

describe("DatetimeFormatter", () => {
  const testDate = dayjs("2024-03-20T14:30:00Z");

  test("formats local time in French", () => {
    const formatter = new DatetimeFormatter("fr-FR");
    expect(formatter.formatLocalTime(testDate)).toMatch(/14:30/);
  });

  test("formats local time in English (12-hour format)", () => {
    const formatter = new DatetimeFormatter("en-GB");
    expect(formatter.formatLocalTime(testDate)).toMatch(/2:30/);
  });

  test("formats local date in French", () => {
    const formatter = new DatetimeFormatter("fr-FR");
    expect(formatter.formatLocalDate(testDate)).toMatch(/20 mars 2024/);
  });

  test("formats local date in English", () => {
    const formatter = new DatetimeFormatter("en-GB");
    expect(formatter.formatLocalDate(testDate)).toMatch(/20 March 2024/);
  });

  test("formats local datetime with different styles", () => {
    const formatter = new DatetimeFormatter("fr-FR");
    const fullFormat = formatter.formatLocalDateTime(testDate, DateFormatStyle.ICU_FULL, TimeFormatStyle.ICU_FULL);
    expect(fullFormat).toMatch(/mercredi 20 mars 2024/);
    expect(fullFormat).toMatch(/14:30/);
  });
});
