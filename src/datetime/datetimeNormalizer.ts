import dayjs, { type Dayjs } from "dayjs";
import utc from "dayjs/plugin/utc";

dayjs.extend(utc);

interface ZonedDateTime {
  dateTime: string;
  timeZone: string;
}

export const DatetimeNormalizer = {
  fromString(str: string) {
    return dayjs.utc(str);
  },

  toString(datetime: Dayjs) {
    return datetime.format("YYYY-MM-DDTHH:mm:ss:SSS");
  },

  fromObject(datetime: ZonedDateTime) {
    return dayjs.tz(datetime.dateTime, datetime.timeZone);
  },

  toObject(datetime: Dayjs) {
    type DayjsZonedDateTime = Dayjs & { $x: { $timezone: string } };

    return {
      dateTime: DatetimeNormalizer.toString(datetime),
      timeZone: (datetime as DayjsZonedDateTime).$x.$timezone,
    };
  },
};
