import dayjs, { type Dayjs } from "dayjs";
import tz from "dayjs/plugin/timezone";
import { i18n } from "@/config";

dayjs.extend(tz);

export enum DateFormatStyle {
  ICU_FULL = "full",
  ICU_LONG = "long",
  ICU_SHORT = "short",
}

export enum TimeFormatStyle {
  ICU_FULL = "full",
  ICU_LONG = "long",
  ICU_SHORT = "short",
}

export default class DatetimeFormatter {
  constructor(private locale = i18n.config.globalProperties.locale) {}

  setLocale(locale: string) {
    this.locale = locale;
    return this;
  }

  private shouldUse12HourFormat(): boolean {
    return i18n.config.use12HourFormat.includes(this.locale);
  }

  formatLocalTime(datetime: Dayjs, timeFormatStyle = TimeFormatStyle.ICU_SHORT) {
    return Intl.DateTimeFormat(this.locale, {
      timeStyle: timeFormatStyle,
      hour12: this.shouldUse12HourFormat(),
    }).format(datetime.toDate());
  }

  formatLocalDate(datetime: Dayjs, dateFormatStyle = DateFormatStyle.ICU_LONG) {
    return Intl.DateTimeFormat(this.locale, {
      dateStyle: dateFormatStyle,
    }).format(datetime.toDate());
  }

  formatLocalDateTime(
    datetime: Dayjs,
    dateFormatStyle = DateFormatStyle.ICU_LONG,
    timeFormatStyle = TimeFormatStyle.ICU_SHORT,
  ) {
    return Intl.DateTimeFormat(this.locale, {
      dateStyle: dateFormatStyle,
      timeStyle: timeFormatStyle,
      hour12: this.shouldUse12HourFormat(),
    }).format(datetime.toDate());
  }

  formatZonedDateTime(
    datetime: Dayjs,
    dateFormatStyle = DateFormatStyle.ICU_LONG,
    timeFormatStyle = TimeFormatStyle.ICU_SHORT,
  ) {
    type ZonedDateTime = Dayjs & { $x: { $timezone: string } };

    return Intl.DateTimeFormat(this.locale, {
      dateStyle: dateFormatStyle,
      timeStyle: timeFormatStyle,
      timeZone: (datetime as ZonedDateTime).$x.$timezone,
      hour12: this.shouldUse12HourFormat(),
    }).format(datetime.toDate());
  }
}
