import decimal, { type Decimal } from "decimal.js";

export default class Percentage {
  constructor(public value: Decimal) {
    if (value.greaterThan(100) || value.lessThan(-100)) {
      throw new Error(`Percentage must to be between -100 and 100, "${value.toNumber()}" given`);
    }
  }

  static fromString(str: string) {
    return new Percentage(new decimal(Number.parseFloat(str)));
  }

  toString() {
    return `${this.value.toNumber().toFixed(2)}`;
  }
}
