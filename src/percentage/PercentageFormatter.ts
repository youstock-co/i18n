import { i18n } from "..";
import type Percentage from "./percentage";

export default class PercentageFormatter {
  constructor(private locale = i18n.config.globalProperties.locale) {}

  setLocale(locale: string) {
    this.locale = locale;
    return this;
  }

  format(percentage: Percentage) {
    // Intl require a fractional number, eg: 0.20 (20/100) => 20%
    return Intl.NumberFormat(this.locale, {
      style: "percent",
      minimumFractionDigits: 2,
    }).format(percentage.value.toNumber() / 100);
  }
}
