import { i18n } from "@/config";
import type Area from "@/measures/impl/area";
import type Distance from "@/measures/impl/distance";
import type Volume from "@/measures/impl/volume";

export default class SiFormatter {
  constructor(private locale: string = i18n.config.globalProperties.locale) {}

  setLocale(locale: string) {
    this.locale = locale;
    return this;
  }

  format(measure: Area | Distance | Volume) {
    const value = Intl.NumberFormat(this.locale, { minimumFractionDigits: 2 }).format(measure.value.toNumber());
    return `${value} ${measure.symbol}`;
  }
}
