import { expect, test, describe } from "bun:test";
import SiFormatter from "@/measures/formatter/siFormatter";
import Distance, { DistanceUnit } from "@/measures/impl/distance";
import Volume, { VolumeUnit } from "@/measures/impl/volume";
import Area, { AreaUnit } from "@/measures/impl/area";
import Decimal from "decimal.js";

describe("SiFormatter", () => {
  test("formats area measure", () => {
    const area = new Area(new Decimal(123.45), AreaUnit.SQUARE_METRE);
    const formatter = new SiFormatter("fr-FR");
    expect(formatter.format(area)).toBe("123,45 m²");
  });

  test("formats distance measure", () => {
    const distance = new Distance(new Decimal(123.45), DistanceUnit.METRE);
    const formatter = new SiFormatter("fr-FR");
    expect(formatter.format(distance)).toBe("123,45 m");
  });

  test("formats volume measure", () => {
    const volume = new Volume(new Decimal(123.45), VolumeUnit.CUBIC_METRE);
    const formatter = new SiFormatter("fr-FR");
    expect(formatter.format(volume)).toBe("123,45 m³");
  });

  test("respects locale in number formatting", () => {
    const area = new Area(new Decimal(123.45), AreaUnit.SQUARE_METRE);
    const formatter = new SiFormatter("en-GB");
    expect(formatter.format(area)).toBe("123.45 m²");
  });
});
