import type Decimal from "decimal.js";

export class MathDivisionArguments {
  constructor(
    public readonly denominator: Decimal,
    public readonly scale: number,
    public readonly roundingMode: Decimal.Rounding,
  ) {}
}
