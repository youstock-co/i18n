import decimal, { type Decimal } from "decimal.js";
import { SIMeasureType } from "../siMeasureType";
import { InvalidUnitError } from "../invalidUnitError";
import AbstractMeasure from "./abstractMeasure";

export enum DistanceUnit {
  CENTIMETRE = "CENTIMETRE",
  METRE = "METRE",
  FOOT = "FOOT",
  INCH = "INCH",
}

export const DistanceSymbols = {
  [DistanceUnit.CENTIMETRE]: "cm",
  [DistanceUnit.METRE]: "m",
  [DistanceUnit.FOOT]: "ft",
  [DistanceUnit.INCH]: "in",
};

export default class Distance extends AbstractMeasure<DistanceUnit> {
  constructor(value: Decimal, unit: DistanceUnit) {
    if (DistanceSymbols[unit] === undefined) throw new InvalidUnitError(`Distance unit not supported, "${unit}" given`);
    super(value, unit);
  }

  get symbol() {
    return DistanceSymbols[this.unit];
  }

  get measureType() {
    return SIMeasureType.DISTANCE;
  }

  static fromString(str: string) {
    const [value, unit] = str.split(" ");

    if (DistanceUnit[unit as DistanceUnit] === undefined) throw Error(`Unsupported distance unit "${unit}"`);

    return new Distance(new decimal(value), unit as DistanceUnit);
  }
}
