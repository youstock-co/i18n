import type { Decimal } from "decimal.js";

export default abstract class Measure<U> {
  constructor(
    public readonly value: Decimal,
    public readonly unit: U,
  ) {}

  abstract get symbol(): string;

  abstract get measureType(): string;

  toString() {
    return `${this.value.toFixed(3)} ${this.unit}`;
  }
}
