import decimal, { Decimal } from "decimal.js";
import { SIMeasureType } from "../siMeasureType";
import { InvalidUnitError } from "../invalidUnitError";
import AbstractMeasure from "./abstractMeasure";

export enum AreaUnit {
  SQUARE_FOOT = "SQUARE_FOOT",
  SQUARE_METRE = "SQUARE_METRE",
}

export const AreaSymbols = {
  [AreaUnit.SQUARE_FOOT]: "sq ft",
  [AreaUnit.SQUARE_METRE]: "m²",
};

export const AreaUnitMultiplier: { [k in AreaUnit]: Decimal } = {
  [AreaUnit.SQUARE_FOOT]: new Decimal("0.09290304"),
  [AreaUnit.SQUARE_METRE]: new Decimal("1"),
};

export default class Area extends AbstractMeasure<AreaUnit> {
  constructor(value: Decimal, unit: AreaUnit) {
    if (AreaSymbols[unit] === undefined) throw new InvalidUnitError(`Area unit not supported, "${unit}" given`);
    super(value, unit);
  }

  get symbol() {
    return AreaSymbols[this.unit];
  }

  get measureType() {
    return SIMeasureType.AREA;
  }

  public toSquareMetre(): Area {
    return new Area(this.value.mul(AreaUnitMultiplier[this.unit]), AreaUnit.SQUARE_METRE);
  }

  public convertTo(unit: AreaUnit): Area {
    return new Area(
      this.value
        .mul(AreaUnitMultiplier[this.unit])
        .div(AreaUnitMultiplier[unit])
        .toDecimalPlaces(9, Decimal.ROUND_HALF_DOWN),
      unit,
    );
  }

  static fromString(str: string) {
    const [value, unit] = str.split(" ");

    if (AreaUnit[unit as AreaUnit] === undefined) throw new InvalidUnitError(`Unsupported area unit "${unit}"`);

    return new Area(new decimal(value), unit as AreaUnit);
  }
}
