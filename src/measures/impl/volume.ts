import decimal, { Decimal } from "decimal.js";
import { SIMeasureType } from "../siMeasureType";
import { InvalidUnitError } from "../invalidUnitError";
import AbstractMeasure from "./abstractMeasure";

export enum VolumeUnit {
  CUBIC_METRE = "CUBIC_METRE",
  CUBIC_FOOT = "CUBIC_FOOT",
}

export const VolumeSymbols = {
  [VolumeUnit.CUBIC_METRE]: "m³",
  [VolumeUnit.CUBIC_FOOT]: "ft³",
};

export const VolumeUnitMultiplier: { [k in VolumeUnit]: Decimal } = {
  [VolumeUnit.CUBIC_FOOT]: new Decimal("0.0283168466"),
  [VolumeUnit.CUBIC_METRE]: new Decimal("1"),
};

export default class Volume extends AbstractMeasure<VolumeUnit> {
  constructor(value: Decimal, unit: VolumeUnit) {
    if (VolumeSymbols[unit] === undefined) throw new InvalidUnitError(`Volume unit not supported, "${unit}" given`);
    super(value, unit);
  }

  get symbol() {
    return VolumeSymbols[this.unit];
  }

  get measureType() {
    return SIMeasureType.VOLUME;
  }

  public toCubicMetre(): Volume {
    return new Volume(this.value.mul(VolumeUnitMultiplier[this.unit]), VolumeUnit.CUBIC_METRE);
  }

  public convertTo(unit: VolumeUnit): Volume {
    return new Volume(
      this.value
        .mul(VolumeUnitMultiplier[this.unit])
        .div(VolumeUnitMultiplier[unit])
        .toDecimalPlaces(9, Decimal.ROUND_HALF_DOWN),
      unit,
    );
  }

  static fromString(str: string) {
    const [value, unit] = str.split(" ");

    if (VolumeUnit[unit as VolumeUnit] === undefined) throw new InvalidUnitError(`Unsupported volume unit "${unit}"`);

    return new Volume(new decimal(value), unit as VolumeUnit);
  }
}
