import type { AreaUnit, VolumeUnit } from "../index";
import type { MathDivisionArguments } from "./mathDivisionArguments";

export class VolumeAreaConverterMappingEntry {
  constructor(
    public readonly areaUnit: AreaUnit,
    public readonly volumeUnit: VolumeUnit,
    public readonly volumeToAreaDivisionArguments: MathDivisionArguments,
  ) {}
}

export type VolumeAreaConverterMappingEntries = VolumeAreaConverterMappingEntry[];
