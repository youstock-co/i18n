import Area, { type AreaUnit } from "./impl/area";
import Volume, { type VolumeUnit } from "./impl/volume";
import { SIMeasureType } from "./siMeasureType";
import type {
  VolumeAreaConverterMappingEntries,
  VolumeAreaConverterMappingEntry,
} from "./volumeAreaConverterMappingEntry";
import { VolumeAreaConverterMappingEntryNotFountError } from "./volumeAreaConverterMappingEntryNotFountError";
import type { CoveredCountry } from "@/country/coveredCountries";

export class VolumeAreaConverter {
  public constructor(
    private readonly country: CoveredCountry,
    private readonly mappingEntries: VolumeAreaConverterMappingEntries,
    private readonly defaultVolumeUnit: VolumeUnit,
    private readonly defaultAreaUnit: AreaUnit,
  ) {}

  public amountToMeasure(amount: Area | Volume, measureType: SIMeasureType): Area | Volume {
    return measureType === SIMeasureType.AREA ? this.amountToArea(amount) : this.amountToVolume(amount);
  }

  public amountToVolume(amount: Area | Volume): Volume {
    return amount instanceof Volume ? amount : this.areaToVolume(amount);
  }

  public amountToArea(amount: Area | Volume): Area {
    return amount instanceof Area ? amount : this.volumeToArea(amount);
  }

  public areaToVolume(area: Area, unit?: VolumeUnit): Volume {
    const volumeUnit = unit || this.defaultVolumeUnit;
    const mappingEntry = this.findMappingEntry(area.unit, volumeUnit);

    return new Volume(area.value.mul(mappingEntry.volumeToAreaDivisionArguments.denominator), volumeUnit);
  }

  public volumeToArea(volume: Volume, unit?: AreaUnit): Area {
    const areaUnit = unit || this.defaultAreaUnit;
    const mappingEntry = this.findMappingEntry(areaUnit, volume.unit);

    return new Area(
      volume.value
        .div(mappingEntry.volumeToAreaDivisionArguments.denominator)
        .toDecimalPlaces(
          mappingEntry.volumeToAreaDivisionArguments.scale,
          mappingEntry.volumeToAreaDivisionArguments.roundingMode,
        ),
      areaUnit,
    );
  }

  private findMappingEntry(areaUnit: AreaUnit, volumeUnit: VolumeUnit): VolumeAreaConverterMappingEntry {
    const mappingEntry = this.mappingEntries.find(
      (entry) => entry.areaUnit === areaUnit && entry.volumeUnit === volumeUnit,
    );
    if (!mappingEntry) {
      throw new VolumeAreaConverterMappingEntryNotFountError(
        `Mapping entry not found for country: ${this.country}, areaUnit: ${areaUnit}, volumeUnit: ${volumeUnit}`,
      );
    }
    return mappingEntry;
  }
}
