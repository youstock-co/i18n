import Area from "../impl/area";
import Volume from "../impl/volume";
import { InvalidUnitError } from "../invalidUnitError";

export const AreaOrVolumeDenormalizer = {
  denormalize(areaOrVolume: string): Area | Volume {
    try {
      return Area.fromString(areaOrVolume);
    } catch (error) {
      if (!(error instanceof InvalidUnitError)) {
        throw error;
      }
    }

    try {
      return Volume.fromString(areaOrVolume);
    } catch (error) {
      if (!(error instanceof InvalidUnitError)) {
        throw error;
      }
    }

    throw new Error(`Unknown areaOrVolume: "${areaOrVolume}"`);
  },
};
