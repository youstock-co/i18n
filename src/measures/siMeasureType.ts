import Area from "@/measures/impl/area";
import Distance from "@/measures/impl/distance";
import Volume from "@/measures/impl/volume";

export enum SIMeasureType {
  AREA = "AREA",
  DISTANCE = "DISTANCE",
  VOLUME = "VOLUME",
}

type GetSiMeasureClass = {
  (type: SIMeasureType.AREA): typeof Area;
  (type: SIMeasureType.VOLUME): typeof Volume;
  (type: SIMeasureType.DISTANCE): typeof Distance;
  (type: SIMeasureType): typeof Area | typeof Volume | typeof Distance;
};

export const getSiMeasureClass = ((type: SIMeasureType): typeof Area | typeof Volume | typeof Distance => {
  switch (type) {
    case SIMeasureType.AREA:
      return Area;
    case SIMeasureType.VOLUME:
      return Volume;
    case SIMeasureType.DISTANCE:
      return Distance;
    default:
      throw new Error("Invalid SIMeasureType");
  }
}) as GetSiMeasureClass;
