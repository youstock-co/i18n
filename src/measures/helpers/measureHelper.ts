import Distance from "../impl/distance";
import Volume from "../impl/volume";
import Area from "../impl/area";
import { SIMeasureType } from "../siMeasureType";

/**
 * @deprecated Use AreaOrVolumeDenormalizer instead
 */
export const MeasureHelper = {
  fromStringByType(type: SIMeasureType, str: string): Area | Distance | Volume {
    switch (type) {
      case SIMeasureType.AREA:
        return Area.fromString(str);
      case SIMeasureType.DISTANCE:
        return Distance.fromString(str);
      case SIMeasureType.VOLUME:
        return Volume.fromString(str);
      default:
        throw new Error("Unknown SIMeasureType");
    }
  },
};
