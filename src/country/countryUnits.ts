import type { Currency } from "dinero.js";
import { type AreaUnit, type DistanceUnit, SIMeasureType, type VolumeUnit } from "..";

export class CountryUnits {
  /**
   * @param transportVolumeUnit @depreacated Use defaultItemMeasureType/defaultItemAreaUnit/defaultItemVolumeUnit instead
   * @param storageVolumeUnit @depreacated Use defaultItemMeasureType/defaultItemAreaUnit/defaultItemVolumeUnit instead
   */
  constructor(
    public readonly currency: Currency<number>,
    public readonly defaultItemMeasureType: SIMeasureType,
    public readonly itemReferenceAreaUnit: AreaUnit,
    public readonly itemReferenceVolumeUnit: VolumeUnit,
    public readonly carryingReferenceDistanceUnit: DistanceUnit,

    public readonly palletDimensionsReferenceDistanceUnit: DistanceUnit,

    public readonly transportVolumeUnit: VolumeUnit,
    public readonly storageVolumeUnit: VolumeUnit,
  ) {}

  public getDefaultUnitOfMeasureType(measureType: SIMeasureType): AreaUnit | VolumeUnit {
    if (measureType === SIMeasureType.AREA) {
      return this.itemReferenceAreaUnit;
    }

    return this.itemReferenceVolumeUnit;
  }
}
