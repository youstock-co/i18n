import { AreaUnit } from "@/measures/impl/area";
import { VolumeUnit } from "@/measures/impl/volume";
import { DistanceUnit } from "@/measures/impl/distance";
import { EUR, GBP } from "@dinero.js/currencies";
import { CountryUnits } from "./countryUnits";
import { SIMeasureType } from "@/measures/siMeasureType";
import { VolumeAreaConverter } from "@/measures/volumeAreaConverter";
import Decimal from "decimal.js";
import { VolumeAreaConverterMappingEntry } from "@/measures/volumeAreaConverterMappingEntry";
import { MathDivisionArguments } from "@/measures/mathDivisionArguments";

export enum CoveredCountry {
  FR = "FR",
  BE = "BE",
  MC = "MC",
  GB = "GB",
}

function getDefaultVolumeAreaConverter(country: CoveredCountry): VolumeAreaConverter {
  return new VolumeAreaConverter(
    country,
    [
      new VolumeAreaConverterMappingEntry(
        AreaUnit.SQUARE_METRE,
        VolumeUnit.CUBIC_METRE,
        new MathDivisionArguments(new Decimal(2), 3, Decimal.ROUND_HALF_UP),
      ),
    ],
    VolumeUnit.CUBIC_METRE,
    AreaUnit.SQUARE_METRE,
  );
}

const gbVolumeAreaConverter: VolumeAreaConverter = new VolumeAreaConverter(
  CoveredCountry.GB,
  [
    new VolumeAreaConverterMappingEntry(
      AreaUnit.SQUARE_FOOT,
      VolumeUnit.CUBIC_METRE,
      new MathDivisionArguments(new Decimal("0.2"), 3, Decimal.ROUND_HALF_UP),
    ),
  ],
  VolumeUnit.CUBIC_METRE,
  AreaUnit.SQUARE_FOOT,
);

export const CoveredCountries: {
  [k in CoveredCountry]: {
    value: CoveredCountry;
    defaultZoneId: number;
    iSO3166Alpha2: string;
    defaultTimeZoneRegion: string;
    units: CountryUnits;
    coveredCountries: CoveredCountry[];
    itemsVolumeAreaConverter: VolumeAreaConverter;
  };
} = {
  [CoveredCountry.FR]: {
    value: CoveredCountry.FR,
    defaultZoneId: 3,
    iSO3166Alpha2: "FR",
    defaultTimeZoneRegion: "Europe/Paris",
    units: new CountryUnits(
      EUR,
      SIMeasureType.VOLUME,
      AreaUnit.SQUARE_METRE,
      VolumeUnit.CUBIC_METRE,
      DistanceUnit.METRE,
      DistanceUnit.CENTIMETRE,
      VolumeUnit.CUBIC_METRE,
      VolumeUnit.CUBIC_METRE,
    ),
    coveredCountries: [CoveredCountry.FR, CoveredCountry.MC],
    itemsVolumeAreaConverter: getDefaultVolumeAreaConverter(CoveredCountry.FR),
  },
  [CoveredCountry.BE]: {
    value: CoveredCountry.BE,
    defaultZoneId: 7,
    iSO3166Alpha2: "BE",
    defaultTimeZoneRegion: "Europe/Brussels",
    units: new CountryUnits(
      EUR,
      SIMeasureType.VOLUME,
      AreaUnit.SQUARE_METRE,
      VolumeUnit.CUBIC_METRE,
      DistanceUnit.METRE,
      DistanceUnit.CENTIMETRE,
      VolumeUnit.CUBIC_METRE,
      VolumeUnit.CUBIC_METRE,
    ),
    coveredCountries: [CoveredCountry.BE],
    itemsVolumeAreaConverter: getDefaultVolumeAreaConverter(CoveredCountry.BE),
  },
  [CoveredCountry.MC]: {
    value: CoveredCountry.MC,
    defaultZoneId: 4,
    iSO3166Alpha2: "MC",
    defaultTimeZoneRegion: "Europe/Monaco",
    units: new CountryUnits(
      EUR,
      SIMeasureType.VOLUME,
      AreaUnit.SQUARE_METRE,
      VolumeUnit.CUBIC_METRE,
      DistanceUnit.METRE,
      DistanceUnit.CENTIMETRE,
      VolumeUnit.CUBIC_METRE,
      VolumeUnit.CUBIC_METRE,
    ),
    coveredCountries: [CoveredCountry.FR, CoveredCountry.MC],
    itemsVolumeAreaConverter: getDefaultVolumeAreaConverter(CoveredCountry.MC),
  },
  [CoveredCountry.GB]: {
    value: CoveredCountry.GB,
    defaultZoneId: 8,
    iSO3166Alpha2: "GB",
    defaultTimeZoneRegion: "Europe/London",
    units: new CountryUnits(
      GBP,
      SIMeasureType.AREA,
      AreaUnit.SQUARE_FOOT,
      VolumeUnit.CUBIC_METRE,
      DistanceUnit.METRE,
      DistanceUnit.CENTIMETRE,
      VolumeUnit.CUBIC_METRE,
      VolumeUnit.CUBIC_METRE,
    ),
    coveredCountries: [CoveredCountry.GB],
    itemsVolumeAreaConverter: gbVolumeAreaConverter,
  },
};
