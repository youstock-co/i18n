# @youstock/i18n

Internationalization and formatting utilities for YouStock applications.

## Installation

```bash
# Using git repository with commit hash
bun add git+ssh://git@gitlab.com/youstock/i18n.git#<commit-hash>
```

## Features

### DateTime Formatting
```typescript
import { DatetimeFormatter } from '@youstock/i18n';

const formatter = new DatetimeFormatter('fr-FR');
const date = new Date();

// Different format styles
formatter.formatLocalTime(date);         // "14:30"
formatter.formatLocalDate(date);         // "20 mars 2024"
formatter.formatLocalDateTime(date);     // "20 mars 2024 14:30"
formatter.formatZonedDateTime(date);     // With timezone
```

### Money Formatting
```typescript
import { MoneyFormatter, MoneyNormalizer } from '@youstock/i18n';

const money = MoneyNormalizer.fromString('EUR 350.60');

// Format with locale
new MoneyFormatter('fr-FR').format(money);           // "350,60 €"
new MoneyFormatter('en-GB').format(money);           // "€350.60"

// Format with pricing type
formatter.format(money, 'EXCLUDING_TAX');            // "350,60 € HT"

// Format with recurrence
formatter.format(money, undefined, 'MONTHLY');       // "350,60 € / mois"
```

### SI Measurements
```typescript
import { Area, Distance, Volume, SiFormatter } from '@youstock/i18n';

// Create measures
const area = Area.fromString('22.00 SQUARE_METRE');
const distance = Distance.fromString('35.00 METRE');
const volume = Volume.fromString('5.00 CUBIC_FOOT');

// Format with locale
const formatter = new SiFormatter('fr-FR');
formatter.format(area);      // "22,00 m²"
formatter.format(distance);  // "35,00 m"
formatter.format(volume);    // "5,00 ft³"

// Convert units
area.toSquareMetre();
volume.toCubicMetre();
```

## Development

1. Install dependencies:
```bash
bun install
```

2. Run tests:
```bash
bun test
# or in watch mode
bun test --watch
```

3. Build:
```bash
bun run build
```

## Versioning

The library uses Git commit hashes for versioning. To use a specific version:
```bash
bun add git+ssh://git@gitlab.com/youstock/i18n.git#<commit-hash>
``` 